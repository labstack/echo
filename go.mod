module gitlab.com/labstack/echo

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.2.2
	github.com/valyala/fasttemplate v0.0.0-20170224212429-dcecefd839c4
	gitlab.com/labstack/gommon v0.2.8
	golang.org/x/crypto v0.0.0-20181112202954-3d3f9f413869
)
